## Automated Dog Feeder

The purpose of this project is to create a voice enabled pet feeder that can be controlled using an Alexa skill.

### Main Features

1. Responde to commands that activate the feeder to deliver a certain amout of ounces. i.e. "Alexa feed the dog 2 ounces"
2. Record the times of feeding so that the user can query the system to avoid refeeding i.e. "Alexa when was the last time we fed the dog"
3. Keep track of the amount of food in the feeder and inform the user if reording is required


### Architecture

The project uses:

* The Amazon alexa Service
* AWS lambda
* Amazon AWS IoT
* Mongoose-os running on a NodeMCU board  

The user interacts with the device via an alexa skill running in an Amazon Echo. This skill is implemented as a node.js lambda function that communicates to AWS IoT to relay the user commands.
The feeder is controlled by a NodeMCU board running a mongoose-os app. this app is connnected to the AWS IoT infrastructure to receive messages send by the alexa skill and send notifications to the user.     


## Phases

* Phase 1 - Wire the whole system so that the feeder dispenses food on commands (Done)
* Phase 2 - Set up a lambda function to record feeding times based on confirmation from the mongoose-os app (in progress)
* Phase 3 - implement sensor in feeder to report low quantity of food and send notification

    
### Development Step for phase 1 ( Done)

* Write Alexa skill to interact with board
* Alexa Skills handles feed intention 
* Alexa Skill sends a message to AWS IoT
* Set up mongoose-os in the nodeMCU board
* Have mongoose-os app send and listen for messages in the AWS IoT topics
* Have mongoose-os activate servo based on AWS IoT message 

