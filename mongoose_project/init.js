// This example demonstrates how to react on a button press
// by sending a message to AWS IoT.
//
// See README.md for details.
//
// Load Mongoose OS API
load('api_gpio.js');
load('api_mqtt.js');
load('api_sys.js');
load('api_config.js');

load('api_net.js');
load('api_timer.js');
load('api_pwm.js');

let pin = 0;   // GPIO 0 is typically a 'Flash' button
let servo_pin = 13;

//let servo_stopped = true;

//PWM.set(servo_pin, 50, 5);
//print('motor stopped!');  

GPIO.set_button_handler(pin, GPIO.PULL_UP, GPIO.INT_EDGE_NEG, 50, function(x) {
  let topic = Cfg.get('device.id') + '/button_pressed';
  let message = JSON.stringify({
    total_ram: Sys.total_ram(),
    free_ram: Sys.free_ram()
  });
  let ok = MQTT.pub(topic, message, 1);
  print('Published:', ok ? 'yes' : 'no', 'topic:', topic, 'message:', message);
  handle_message();
}, true);


print('Flash button is configured on GPIO pin ', pin);
print('Press the flash button now!');


MQTT.sub('dogFeederTopic/feed', function(conn, topic, message){
  let messageObj = JSON.parse(message); 
  print('Topic:', topic, 'message:', messageObj.total_ram);
  handle_message();
}, null)

function handle_message() {
    print('motor started!');
    PWM.set(servo_pin, 50, 0);
	//set the timer to stop the motor
	Timer.set(2000, false, function() {
		PWM.set(servo_pin, 50, 1);
		print('motor stopped!');  
	}, null);
}


