var bst = require('bespoken-tools');
var assert = require('assert');

var server = null;
var alexa = null;

beforeEach(function (done) {
    server = new bst.LambdaServer('./index.js', 10000, true);
    alexa = new bst.BSTAlexa('http://localhost:10000',
                         '../speechAssets/IntentSchema.json',
                         '../speechAssets/Utterances.txt');
    server.start(function() {
    alexa.start(function (error) {
          if (error !== undefined) {
            console.error("Error: " + error);
          } else {
              done();
          }
        });
    });
});

it('Launches skill', function (done) {
    // Launch the skill via sending it a LaunchRequest
    alexa.launched(function (error, payload) {
		// Check that the introduction is play as outputSpeech
		assert.equal(payload.response.outputSpeech.ssml, '<speak> OK, I\'ll feed the dog. </speak>');
		done(); 
    });
});

/*
it('Handles One Shot Invocation To Feed The Dog', function (done){
	// Handles user response indicating number of ounces
		
        alexa.intended('FeedPet', null, function (error, payload) {
            assert.equal(payload.response.outputSpeech.ssml, '<speak> OK, I fed the dog </speak>');
            done();
        });
}); 
*/


afterEach(function(done) {
    alexa.stop(function () {
        server.stop(function () {
            done();
        });
    });
});
